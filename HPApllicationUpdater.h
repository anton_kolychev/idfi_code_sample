//
//  HPApllicationUpdater.h
//  Home3D Pro
//
//  Created by Kolychev Anton on 29.12.12.
//  Copyright (c) 2012 Black Mana Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HPApllicationUpdaterDelegate <NSObject>
- (void)updateDocumentWithURL:(NSURL *)documentUrl;
@end

@interface HPApllicationUpdater : NSObject
- (id)initWithDelegate:(id<HPApllicationUpdaterDelegate>)delegate;
- (void)updateIfNeeded;
@property (nonatomic, weak) id<HPApllicationUpdaterDelegate> delegate;
@end
