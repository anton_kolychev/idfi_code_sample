//
//  HPApllicationUpdater.m
//  Home3D Pro
//
//  Created by Kolychev Anton on 29.12.12.
//  Copyright (c) 2012 Black Mana Studios. All rights reserved.
//

#import "HPApllicationUpdater.h"
#import "HPUpdateFrom1_0To1_1.h"

#define kUpdateSampleVersion1_1 @"updateSampleTo1.1"

@interface HPApllicationUpdater() <HPUpdateFrom1_0To1_1Delagete>
@property (nonatomic, strong) HPUpdateFrom1_0To1_1 *updaterTo1_1;
@end

@implementation HPApllicationUpdater
@synthesize delegate = _delegate;
@synthesize updaterTo1_1 = _updaterTo1_1;

- (id)initWithDelegate:(id<HPApllicationUpdaterDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        _delegate = delegate;
    }
    return self;
}

- (void)updateIfNeeded
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    if ([preferences boolForKey:kUpdateSampleVersion1_1] == NO)
    {
        self.updaterTo1_1 = [[HPUpdateFrom1_0To1_1 alloc] initWithDelegate:self];
        [self.updaterTo1_1 update];
        [preferences setBool:YES forKey:kUpdateSampleVersion1_1];
        [preferences synchronize];
    }
}

#pragma mark - HPUpdateFrom1_0To1_1Delagete

- (void)updateDocumentWithURL:(NSURL *)documentUrl
{
    [self.delegate updateDocumentWithURL:documentUrl];
}

@end
